
/**
*	Autor: José Javier Jo,  Rodrigo Custodio 
*	Descripcion: Index.js de las rutas
*/

const routes = require('./routes');

module.exports = (app, client) => {
	routes(app, client);
}
