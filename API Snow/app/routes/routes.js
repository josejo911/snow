/**
*	Autor: José Javier Jo, Rodrigo Custodio
*	Descripcion: Handler de las rutas
*/

const formidable = require('formidable');
const uuidV4 = require('uuid/v4');
const path = require('path');
const fs = require('fs');

module.exports = function(app, client) {

	// API para subir un tweet al server
	app.post('/upload_tweet', (req, res) => {


		let form = new formidable.IncomingForm();
		let tweet_text = '';
		let ext = '';
		let success = false;

		// Decimos que queremos mantener la extension de la imagen
		form.keepExtensions = false;

		// Genramos un ID unico para la imagen
		const id = uuidV4();

		form.parse(req);

		// Verificamos el field de texto
		form.on('field', function(name, value) {
			if(name == 'tweet'){
				tweet_text = value;
			}
		});

		// Momento de detectar una imagen en el request
		form.on('fileBegin', (name, file) => {
			// Establecemos la direccion a guardar la imagen en el fs
			ext = path.extname(file.name);
			file.path = __dirname + '/../images/' + id + ext;
		});

		// Si todo se ejecuta con exito procedemos a insertar todo a la base de datos
		form.on('end', () => {
			let query = 'INSERT INTO tweets (tweet_id, tweet_text, image_ext, date_posted) VALUES (?, ?, ?, ?);'
			let params = [id, tweet_text, ext, new Date()];
			client.execute(query, params, { prepare: true })
				.then(result => res.send('OK'));
		});

	});

	// API para mostrar los tweets en JSON
	app.get('/tweets', (req, res) => {

		let max = req.query.max;
		let flag = req.query.flag;
		let query = 'SELECT * FROM tweets';
		let params = [];
		let output = {data: []};
		client.eachRow(query, params, { autoPage: true }, (n, row) => {
			let tweet = {
				tweet_id: row.tweet_id,
				tweet_text: row.tweet_text,
			   	image_ext: row.image_ext,
			   	date_posted: row.date_posted
			};
			output.data.push(tweet);
		}, 
		(err) => {
			if (!err) {
				res.contentType('application/json');
				res.send(JSON.stringify(output));
			}
		});

	});

};
