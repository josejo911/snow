
/*
*	Autor: José Javier Jo, Emilio Diaz
*	Descripcion: API De imagenes
*/

const express = require('express');
const bodyParser = require('body-parser');
const cassandra = require('cassandra-driver');
const db = require('./app/config/db');
const app = express();

const client = new cassandra.Client({ contactPoints: [db.url], keyspace: db.keyspace });

const port = 80;


// Agregamos el acceso esatico a las imagenes
app.use('/images', express.static('./app/images'));
//app.use(bodyParser.json());

client.connect(function (err) {
	if (err) return console.error(err);


	require('./app/routes')(app, client);

	app.listen(port, function() {
		console.log('REST APIs -> puerto:' + port);
	});
});
// Funcion que ejecuta las 
